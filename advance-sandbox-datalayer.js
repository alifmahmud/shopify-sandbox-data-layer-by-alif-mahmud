(function(w, d, s, l, i) {
    w[l] = w[l] || [];
    w[l].push({ 'gtm.start': new Date().getTime(), event: 'gtm.js' });
    var f = d.getElementsByTagName(s)[0],
        j = d.createElement(s),
        dl = l != 'dataLayer' ? '&l=' + l : '';
    j.async = true;
    j.src = 'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
    f.parentNode.insertBefore(j, f);
})(window, document, 'script', 'dataLayer', 'GTM-1234567');

// Helper function to get a specific cookie value by name
const getCookieValue = async (cookieName) => {
    const cookie = await browser.cookie.get(cookieName);
    return cookie || '';
};

// Function to retrieve and update consent choice based on _cmp_a cookie
const getConsentChoice = async () => {
    const cmpCookieValue = await getCookieValue('_cmp_a');
    let consentChoice = {
        ad_storage: "denied",
        ad_user_data: "denied",
        ad_personalization: "denied",
        analytics_storage: "denied",
        functionality_storage: "denied",
        personalization_storage: "denied",
        security_storage: "granted"
    };

    if (cmpCookieValue) {
        let cmpData = JSON.parse(decodeURIComponent(cmpCookieValue));
        consentChoice = {
            ad_storage: cmpData.purposes.m ? "granted" : "denied",
            ad_user_data: cmpData.purposes.m ? "granted" : "denied",
            ad_personalization: cmpData.purposes.m ? "granted" : "denied",
            analytics_storage: cmpData.purposes.a ? "granted" : "denied",
            functionality_storage: cmpData.purposes.p ? "granted" : "denied",
            personalization_storage: cmpData.purposes.p ? "granted" : "denied",
            security_storage: "granted"
        };

        console.log("Updated consentChoice:", consentChoice);
    } else {
        console.log("_cmp_a cookie not found.");
    }

    return consentChoice;
};

// Function to handle Google Tag Manager consent updates
const handleGtag = async () => {
    const consentChoice = await getConsentChoice();

    if (typeof dataLayer === 'undefined') {
        console.error('dataLayer is not defined');
        return;
    }

    const gtag = function() {
        dataLayer.push(arguments);
    };

    if (consentChoice) {
        gtag('consent', 'update', consentChoice);
        console.log("Gtag consent updated:", consentChoice);
    } else {
        console.error("consentChoice not available.");
    }
};

// Call the function to handle GTM consent updates
handleGtag();

// Get marketing cookies
const getAllCookies = async () => {
    return {
        facebook: {
            fbp: await getCookieValue('_fbp'),
            fbc: await getCookieValue('_fbc')
        },
        googleAds: {
            IDE: await getCookieValue('IDE'),
            gclid: await getCookieValue('gclid'),
            gcl_aw: await getCookieValue('_gcl_aw'),
            ANID: await getCookieValue('ANID')
        },
        googleAnalytics: {
            ga: await getCookieValue('_ga'),
            gid: await getCookieValue('_gid'),
            ga_W6NZKCQD0R: await getCookieValue('_ga_W6NZKCQD0R'),
            gat: await getCookieValue('_gat')
        },
        tiktok: {
            tt_enable_cookie: await getCookieValue('_tt_enable_cookie'),
            ttclid: await getCookieValue('ttclid'),
            ttp: await getCookieValue('_ttp')
        },
        snapchat: {
            scid: await getCookieValue('_scid'),
            sctr: await getCookieValue('_sctr')
        },
        pinterest: {
            pinterest_sess: await getCookieValue('_pinterest_sess'),
            pinterest_ct: await getCookieValue('_pinterest_ct')
        },
        linkedIn: {
            li_sugr: await getCookieValue('li_sugr'),
            li_at: await getCookieValue('li_at'),
            liap: await getCookieValue('liap')
        }
    };
};

// Helper function to log dataLayer pushes
function logDataLayerPush(data) {
    const styles = [
        'background: linear-gradient(to right, #4CAF50, #8BC34A)',
        'color: white',
        'font-weight: bold',
        'padding: 2px 4px',
        'border-radius: 3px'
    ].join(';');

    const message = '%cdataLayer:';
    console.log(message, styles, data);
    window.dataLayer.push(data);
}

// Mapping of currency to country
const currencyToCountry = {
    'USD': 'US', 'EUR': 'NL', 'BDT': 'BD', 'GBP': 'GB', 'CAD': 'CA', 'AUD': 'AU', 'JPY': 'JP',
    'INR': 'IN', 'CNY': 'CN', 'SEK': 'SE', 'NOK': 'NO', 'CHF': 'CH', 'SGD': 'SG', 'BRL': 'BR',
    'ZAR': 'ZA', 'RUB': 'RU', 'AED': 'AE', 'KRW': 'KR', 'MXN': 'MX', 'TRY': 'TR', 'MYR': 'MY',
    'NZD': 'NZ', 'HKD': 'HK', 'DKK': 'DK', 'PLN': 'PL', 'THB': 'TH', 'EGP': 'EG', 'ILS': 'IL',
    'IDR': 'ID', 'CZK': 'CZ', 'HUF': 'HU', 'ARS': 'AR', 'CLP': 'CL', 'PHP': 'PH', 'COP': 'CO',
    'RSD': 'RS', 'QAR': 'QA', 'BHD': 'BH', 'KWD': 'KW', 'OMR': 'OM', 'PKR': 'PK', 'SAR': 'SA'
};

// Function to create items array
function createItemsArray(products) {
    return products.map(product => ({
        item_id: product.product.id,
        item_variant: product.id,
        item_name: product.product.title,
        item_category: product.product.type,
        item_brand: product.product.vendor,
        item_delivery: 'in_store',
        price: product.price.amount,
        quantity: product.quantity || 1
    }));
}

// Function to create gads items
function createGadsItems(itemsArray, countryCode) {
    return itemsArray.map(product => ({
        id: 'shopify_' + countryCode + '_' + product.item_id + '_' + product.item_variant,
        google_business_vertical: 'retail'
    }));
}

// Function to create unique categories
function createUniqueCategories(itemsArray) {
    return [...new Set(itemsArray.map(product => product.item_category))];
}

// Function to create content name
function createContentName(itemsArray) {
    return itemsArray.length === 1 ? itemsArray[0].item_name : itemsArray.map(product => product.item_name);
}

// Function to create content ID
function createContentId(itemsArray) {
    return itemsArray.length === 1 ? itemsArray[0].item_id : itemsArray.map(product => product.item_id);
}

// Function to create data for events
function createEventData(event, itemsArray, total, currency, cookies, dataType) {
    const countryCode = currencyToCountry[currency] || 'US';
    const gadsItems = createGadsItems(itemsArray, countryCode);
    const uniqueCategories = createUniqueCategories(itemsArray);
    const contentName = createContentName(itemsArray);
    const contentId = createContentId(itemsArray);

    const eventData = {
        event: dataType,
        client_id: event.clientId,
        event_id: event.id,
        time_stamp: event.timestamp,
        time_stamp_unix: Math.round(new Date(event.timestamp).getTime() / 1000),
        page_details: {
            location: event.context.document.location.href,
            path: event.context.document.location.pathname,
            hostname: event.context.document.location.hostname
        },
        cookies: cookies,
        setupBy: 'alifmahmud.com',
        ecommerce: {
            value: total,
            currency: currency,
            items: itemsArray
        },
        user_data: {
            email: event.data.checkout?.email || '',
            phone: event.data.checkout?.phone || '',
            first_name: event.data.checkout?.shippingAddress?.firstName || '',
            last_name: event.data.checkout?.shippingAddress?.lastName || '',
            address: event.data.checkout?.shippingAddress?.address1 || '',
            city: event.data.checkout?.shippingAddress?.city || '',
            province: event.data.checkout?.shippingAddress?.province || '',
            zip: event.data.checkout?.shippingAddress?.zip || '',
            country: event.data.checkout?.shippingAddress?.country || ''
        },
        advertising_platforms: {
            google_ads: {
                remarketing: gadsItems
            },
            facebook: {
                content_type: 'product_group',
                content_ids: contentId,
                content_name: contentName,
                content_category: uniqueCategories.join(', '),
                value: total,
                currency: currency,
                num_items: itemsArray.reduce((acc, item) => acc + item.quantity, 0),
                contents: itemsArray.map(product => ({
                    id: product.item_id,
                    item_price: product.price,
                    delivery_category: product.item_delivery,
                    quantity: product.quantity
                }))
            },
            tiktok: {
                content_type: 'product_group',
                content_id: contentId,
                content_name: contentName,
                content_category: uniqueCategories.join(', '),
                value: total,
                currency: currency,
                quantity: itemsArray.reduce((acc, item) => acc + item.quantity, 0),
                contents: itemsArray.map(product => ({
                    content_id: product.item_id,
                    content_name: product.item_name,
                    content_category: product.item_category,
                    brand: product.item_brand,
                    price: product.price,
                    quantity: product.quantity
                }))
            },
            pinterest: {
                product_ids: contentId,
                product_names: contentName,
                value: total,
                product_quantity: itemsArray.reduce((acc, item) => acc + item.quantity, 0),
                product_category: uniqueCategories.join(', '),
                line_items: itemsArray.map(product => ({
                    product_id: product.item_id,
                    product_name: product.item_name,
                    product_price: product.price,
                    product_brand: product.item_brand,
                    product_quantity: product.quantity,
                    product_category: product.item_category
                }))
            },
            snapchat: {
                item_ids: contentId,
                item_category: uniqueCategories.join(', '),
                price: total,
                number_items: itemsArray.reduce((acc, item) => acc + item.quantity, 0)
            }
        }
    };

    if (dataType === 'purchase') {
        eventData.ecommerce.tax = event.data.checkout?.totalTax?.amount || 0;
        eventData.ecommerce.shipping = event.data.checkout?.shippingLine?.price?.amount || 0;
        eventData.ecommerce.coupon = event.data.checkout?.discountApplications[0]?.title || '';
        eventData.ecommerce.transaction_id = event.data.checkout?.order?.id || '';
    }

    return eventData;
}

// Subscribe to analytics events and log data layer pushes
analytics.subscribe("page_viewed", async (event) => {
    const cookies = await getAllCookies();
    const data = {
        event: 'page_view',
        client_id: event.clientId,
        event_id: event.id,
        time_stamp: event.timestamp,
        time_stamp_unix: Math.round(new Date(event.timestamp).getTime() / 1000),
        page_details: {
            location: event.context.document.location.href,
            path: event.context.document.location.pathname,
            hostname: event.context.document.location.hostname
        },
        cookies: cookies,
        setupBy: 'alifmahmud.com'
    };
    logDataLayerPush(data);
});

analytics.subscribe("collection_viewed", async (event) => {
    const cookies = await getAllCookies();
    const itemsArray = createItemsArray(event.data.collection.productVariants);
    const total = itemsArray.reduce((acc, item) => acc + item.price, 0);
    const currency = event.data.collection.productVariants[0].price.currencyCode;
    const data = createEventData(event, itemsArray, total, currency, cookies, 'view_item_list');
    logDataLayerPush(data);
});

analytics.subscribe("product_viewed", async (event) => {
    const cookies = await getAllCookies();
    const itemsArray = createItemsArray([event.data.productVariant]);
    const total = itemsArray[0].price;
    const currency = event.data.productVariant.price.currencyCode;
    const data = createEventData(event, itemsArray, total, currency, cookies, 'view_item');
    logDataLayerPush(data);
});

analytics.subscribe("product_added_to_cart", async (event) => {
    const cookies = await getAllCookies();
    const itemsArray = createItemsArray([event.data.cartLine.merchandise]);
    const total = event.data.cartLine.cost.totalAmount.amount;
    const currency = event.data.cartLine.cost.totalAmount.currencyCode;
    const data = createEventData(event, itemsArray, total, currency, cookies, 'add_to_cart');
    logDataLayerPush(data);
});

analytics.subscribe("cart_viewed", async (event) => {
    const cookies = await getAllCookies();
    const itemsArray = createItemsArray(event.data.cart.lines.map(line => line.merchandise));
    const total = event.data.cart.cost.totalAmount.amount;
    const currency = event.data.cart.cost.totalAmount.currencyCode;
    const data = createEventData(event, itemsArray, total, currency, cookies, 'view_cart');
    logDataLayerPush(data);
});

analytics.subscribe("product_removed_from_cart", async (event) => {
    const cookies = await getAllCookies();
    const itemsArray = createItemsArray([event.data.cartLine.merchandise]);
    const total = event.data.cartLine.cost.totalAmount.amount;
    const currency = event.data.cartLine.cost.totalAmount.currencyCode;
    const data = createEventData(event, itemsArray, total, currency, cookies, 'remove_from_cart');
    logDataLayerPush(data);
});

analytics.subscribe("checkout_started", async (event) => {
    const cookies = await getAllCookies();
    const itemsArray = createItemsArray(event.data.checkout.lineItems.map(item => item.variant));
    const total = event.data.checkout.totalPrice.amount;
    const currency = event.data.checkout.totalPrice.currencyCode;
    const data = createEventData(event, itemsArray, total, currency, cookies, 'begin_checkout');
    logDataLayerPush(data);
});

analytics.subscribe("checkout_contact_info_submitted", async (event) => {
    const cookies = await getAllCookies();
    const itemsArray = createItemsArray(event.data.checkout.lineItems.map(item => item.variant));
    const total = event.data.checkout.totalPrice.amount;
    const currency = event.data.checkout.totalPrice.currencyCode;
    const data = createEventData(event, itemsArray, total, currency, cookies, 'checkout_contact_info_submitted');
    logDataLayerPush(data);
});

analytics.subscribe("checkout_address_info_submitted", async (event) => {
    const cookies = await getAllCookies();
    const itemsArray = createItemsArray(event.data.checkout.lineItems.map(item => item.variant));
    const total = event.data.checkout.totalPrice.amount;
    const currency = event.data.checkout.totalPrice.currencyCode;
    const data = createEventData(event, itemsArray, total, currency, cookies, 'add_shipping_info');
    logDataLayerPush(data);
});

analytics.subscribe("checkout_shipping_info_submitted", async (event) => {
    const cookies = await getAllCookies();
    const itemsArray = createItemsArray(event.data.checkout.lineItems.map(item => item.variant));
    const total = event.data.checkout.totalPrice.amount;
    const currency = event.data.checkout.totalPrice.currencyCode;
    const data = createEventData(event, itemsArray, total, currency, cookies, 'checkout_shipping_info_submitted');
    logDataLayerPush(data);
});

analytics.subscribe("payment_info_submitted", async (event) => {
    const cookies = await getAllCookies();
    const itemsArray = createItemsArray(event.data.checkout.lineItems.map(item => item.variant));
    const total = event.data.checkout.totalPrice.amount;
    const currency = event.data.checkout.totalPrice.currencyCode;
    const data = createEventData(event, itemsArray, total, currency, cookies, 'add_payment_info');
    logDataLayerPush(data);
});

analytics.subscribe("checkout_completed", async (event) => {
    const cookies = await getAllCookies();
    const itemsArray = createItemsArray(event.data.checkout.lineItems.map(item => item.variant));
    const total = event.data.checkout.totalPrice.amount;
    const currency = event.data.checkout.totalPrice.currencyCode;
    const data = createEventData(event, itemsArray, total, currency, cookies, 'purchase');
    logDataLayerPush(data);
});