const getCookieValue = async (cookieName) => {
    const cookie = await browser.cookie.get(cookieName);
    return cookie || '';
};

const getAllCookies = async () => {
    return {
        facebook: {
            _fbp: await getCookieValue('_fbp'),
            _fbc: await getCookieValue('_fbc')
        },
        googleAds: {
            IDE: await getCookieValue('IDE'),
            ANID: await getCookieValue('ANID')
        },
        googleAnalytics: {
            _ga: await getCookieValue('_ga'),
            _gid: await getCookieValue('_gid'),
            _gat: await getCookieValue('_gat')
        },
        tiktok: {
            _tt_enable_cookie: await getCookieValue('_tt_enable_cookie'),
            _ttp: await getCookieValue('_ttp')
        },
        snapchat: {
            _scid: await getCookieValue('_scid'),
            _sctr: await getCookieValue('_sctr')
        },
        pinterest: {
            _pinterest_sess: await getCookieValue('_pinterest_sess'),
            _pinterest_ct: await getCookieValue('_pinterest_ct')
        },
        linkedIn: {
            li_sugr: await getCookieValue('li_sugr'),
            li_at: await getCookieValue('li_at'),
            liap: await getCookieValue('liap')
        }
    };
};

analytics.subscribe('page_viewed', async (event) => {
    const cookies = await getAllCookies();
    console.log(cookies);
});


//use this to get value inside the customer events

const cookies = await getAllCookies();
console.log(cookies);
