//created by alifmahmud.com & metricsrealm.com

(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TWHHPCNJ');

console.log("tag manager loaded");


//view item list
analytics.subscribe('collection_viewed', (event) => {
    var eventData = event.data.collection;
    
    var ecommerce = {
        currency: eventData.productVariants[0].price.currencyCode,
        value: 0,
        item_list_id: eventData.id,
        item_list_name: eventData.title,
        items: []
    }

    eventData.productVariants.forEach((element)=>{
        var total = 0;
        var tempItems =
            {
                item_id: element.id,
                item_name: element.product.title,
                item_brand: element.product.vendor,
                item_category: element.product.type,
                item_category2: element.sku,
                item_variant_id: element.id,
                price: element.price.amount,
            }

        ecommerce.items.push(tempItems);
        total += Number(element.price.amount)
        ecommerce.value = total;
    })

    window.dataLayer = window.dataLayer || [];
        
        window.dataLayer.push({
            event: "view_item_list",
            ecommerce: ecommerce,
        })

    console.log({
        event: "view_item_list",
        ecommerce: ecommerce,
    })

});
  
//view item
analytics.subscribe('product_viewed', (event) => {
    var eventData = event.data.productVariant;

    var ecommerce = {
        setup: "metricsrealm.com",
        value: eventData.price.amount,
        currency: eventData.price.currencyCode,
        items: [
            {
                item_id: eventData.product.id,
                item_name: eventData.product.title,
                item_brand: eventData.product.vendor,
                item_category: eventData.product.type,
                item_category2: eventData.sku,
                item_variant_id: eventData.id,
                price: eventData.price.amount,
            }
        ],
    }
    window.dataLayer = window.dataLayer || [];

    window.dataLayer.push({
        event: "view_item",
        ecommerce: ecommerce,
    })

    console.log({
        event: "view_item",
        ecommerce: ecommerce,
    });
});


// add to cart
analytics.subscribe('product_added_to_cart', (event) => {
    
    var eventData = event.data.cartLine;

    var ecommerce = {
        setup: "metricsrealm.com",
        value: eventData.cost.totalAmount.amount,
        currency: eventData.cost.totalAmount.currencyCode,
        items: [
            {
                item_id: eventData.merchandise.product.id,
                item_name: eventData.merchandise.product.title,
                item_brand: eventData.merchandise.product.vendor,
                item_category: eventData.merchandise.product.type,
                item_category2: eventData.merchandise.sku,
                item_variant_id: eventData.merchandise.id,
                price: eventData.merchandise.price.amount,
                quantity: eventData.quantity
            }
        ],
    }
    window.dataLayer = window.dataLayer || [];
    
    window.dataLayer.push({
        event: "add_to_cart",
        ecommerce: ecommerce,
    })
    
    console.log({
        event: "add_to_cart",
        ecommerce: ecommerce,
    });
});

//view cart
analytics.subscribe('cart_viewed', (event) => {
    var eventData = event.data.cart;
    var ecommerce = {
        setup: "metricsrealm.com",
        currency: eventData.cost.totalAmount.amount,
        value: eventData.cost.totalAmount.currencyCode,
        items: []
    }

    eventData.lines.forEach((element)=>{
        var tempItems =
            {
                item_id: element.merchandise.product.id,
                item_name: element.merchandise.product.title,
                item_brand: element.merchandise.product.vendor,
                item_category: element.merchandise.product.type,
                item_category2: element.merchandise.sku,
                item_variant_id: element.merchandise.id,
                price: element.merchandise.price.amount,
                quantity: element.quantity
            }

        ecommerce.items.push(tempItems);
    })

    window.dataLayer = window.dataLayer || [];
        
        window.dataLayer.push({
            event: "view_cart",
            ecommerce: ecommerce,
        })

    console.log({
        event: "view_cart",
        ecommerce: ecommerce,
    })
});

//remove from cart
analytics.subscribe('product_removed_from_cart', (event) => {
    var eventData = event.data.cartLine;
    
    var ecommerce = {
        setup: "metricsrealm.com",
        currency: eventData.cost.totalAmount.currencyCode,
        value: eventData.cost.totalAmount.amount,
        items: []
    }
    
    
    var tempItems = {
            item_id: eventData.merchandise.product.id,
            item_name: eventData.merchandise.product.untranslatedTitle.split(" -")[0],
            item_brand: eventData.merchandise.product.vendor,
            item_category: eventData.merchandise.product.type,
            item_category2: eventData.merchandise.sku,
            item_variant_id: eventData.merchandise.id,
            price: eventData.merchandise.price.amount,
            quantity: eventData.quantity
    }
    
    ecommerce.items.push(tempItems);
    
    window.dataLayer = window.dataLayer || [];
        
        window.dataLayer.push({
            event: "remove_from_cart",
            ecommerce: ecommerce,
        })
    
    console.log({
        event: "remove_from_cart",
        ecommerce: ecommerce,
    })
});

//begin checkout
analytics.subscribe('checkout_started', (event) => {
    var eventData =  event.data.checkout;

    var ecommerce = {
        setup: "metricsrealm.com",
        currency: eventData.totalPrice.currencyCode,
        value: eventData.totalPrice.amount,
        items: []
    }

    eventData.lineItems.forEach((element)=>{
        var tempItems =
            {
                item_id: element.variant.product.id,
                item_name: element.variant.product.title,
                item_brand: element.variant.product.vendor,
                item_category: element.variant.product.type,
                item_category2: element.variant.sku,
                item_variant_id: element.variant.id,
                price: element.variant.price.amount,
                quantity: element.quantity,
            }

        ecommerce.items.push(tempItems);
    })

    window.dataLayer = window.dataLayer || [];
        
        window.dataLayer.push({
            event: "begin_checkout",
            ecommerce: ecommerce,
        })

    console.log({
        event: "begin_checkout",
        ecommerce: ecommerce,
    })
});

//add shipping info
analytics.subscribe('checkout_shipping_info_submitted', (event) => {

    var eventData = event.data.checkout;

    var ecommerce = {
        setup: "metricsrealm.com",
        currency: eventData.totalPrice.currencyCode,
        value: eventData.totalPrice.amount,
        items: []
    }

    eventData.lineItems.forEach((element)=>{
        var tempItems =
            {
                item_id: element.variant.product.id,
                item_name: element.variant.product.title,
                item_brand: element.variant.product.vendor,
                item_category: element.variant.product.type,
                item_category2: element.variant.sku,
                item_variant_id: element.variant.id,
                price: element.variant.price.amount,
                quantity: element.quantity
            }

        ecommerce.items.push(tempItems);
    })

    window.dataLayer = window.dataLayer || [];
        
        window.dataLayer.push({
            event: "add_shipping_info",
            ecommerce: ecommerce,
        })

    console.log({
        event: "add_shipping_info",
        ecommerce: ecommerce,
    })

});

//add payment info
analytics.subscribe('payment_info_submitted', (event) => {

    var eventData = event.data.checkout;
    
    var ecommerce = {
        setup: "metricsrealm.com",
        currency: eventData.totalPrice.currencyCode,
        value: eventData.totalPrice.amount,
        items: []
    }

    eventData.lineItems.forEach((element)=>{
        var tempItems =
            {
                item_id: element.variant.product.id,
                item_name: element.variant.product.title,
                item_brand: element.variant.product.vendor,
                item_category: element.variant.product.type,
                item_category2: element.variant.sku,
                item_variant_id: element.variant.id,
                price: element.variant.price.amount,
                quantity: element.quantity,
            }

        ecommerce.items.push(tempItems);
    })

    window.dataLayer = window.dataLayer || [];
        
        window.dataLayer.push({
            event: "add_payment_info",
            ecommerce: ecommerce,
        })

    console.log({
        event: "add_payment_info",
        ecommerce: ecommerce,
    })

});

//purchase
analytics.subscribe('checkout_completed', (event) => {

var eventData =  event.data.checkout;
    
var userData = {
    email: eventData.email,
    firstName: eventData.billingAddress.firstName,
    lastName: eventData.billingAddress.lastName,
    address1: eventData.billingAddress.address1,
    city: eventData.billingAddress.city,
    state: eventData.billingAddress.province,
    zip: eventData.billingAddress.zip,
    country: eventData.billingAddress.country,
}

var ecommerce = {
    setup: "metricsrealm.com",
    currency: eventData.currencyCode,
    transaction_id: eventData.order.id,
    value: eventData.totalPrice.amount,
    tax: eventData.totalTax.amount,
    shipping: eventData.shippingLine.price.amount,
    coupon: eventData.discountApplications[0] ? eventData.discountApplications[0].title : "",
    items: [],
}

var getLineItems = eventData.lineItems;

getLineItems.forEach((element) => {
    ecommerce.items.push(
        {
            item_id: element.variant.product.id,
            item_name: element.variant.product.title,
            item_brand: element.variant.product.vendor,
            item_category: element.variant.sku,
            item_variant: element.variant.id,
            item_list_name: element.variant.product.type,
            item_list_id: element.variant.product.type.replaceAll(" ","-"),
            price: element.variant.price.amount,
            quantity: element.quantity
        }   
    )
});


window.dataLayer = window.dataLayer || []
window.dataLayer.push({
    event: "purchase",
    userData: userData,
    ecommerce: ecommerce
})

console.log({
    event: "purchase",
    userData: userData,
    ecommerce: ecommerce
});
  
});